package output

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
)

// DumpJSON function to print beautifully json
func DumpJSON(data interface{}) {
	js, err := json.MarshalIndent(data, "", "  ")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(string(js))
	os.Exit(0)
}
