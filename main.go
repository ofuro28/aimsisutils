package main

import (
	"os"

	"bitbucket.org/ofuro28/aimsisutils/formula"
	"bitbucket.org/ofuro28/aimsisutils/output"
	"bitbucket.org/ofuro28/aimsisutils/parse"
	"bitbucket.org/ofuro28/aimsisutils/score"
)

func main() {
	var (
		nodes         []formula.Node
		academicCycle score.AcademicCycle
	)
	scoreFinal := make(map[int]map[string][]*score.Final)

	// Load formula and scores
	formulaPath := os.Args[1]
	scorePath := os.Args[2]
	parse.LoadJSON(formulaPath, &nodes, true)
	parse.LoadJSON(scorePath, &academicCycle, true)

	// Exec the formulas with given scores
	for _, scoreHeader := range academicCycle.ScoreHeaders {
		scoreFinal[scoreHeader.UserID] = formula.ExecNodes(&nodes, scoreHeader, academicCycle.ScoreComponents)
	}

	output.DumpJSON(scoreFinal)
}
