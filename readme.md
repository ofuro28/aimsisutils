# AIMSIS Utils Library
Collection of Shared Utils Library for AIMSIS1.0

## Formula

Supported Operation

- Filtering by it's properties
- Grouping by it's properties
- Joining by using mathematical function
    - Average
    - Max
    - Min
- Mathematical function
    - Round
    - Floor
- Raw Mathematical Operation

Formula Structures

Below is simple formula to get average of all kognitif scores
```json
[{
  "name": "psi_final",
  "input": "score_details",
  "formulas": [
    {
      "name": "psi1_scores",
      "method": "filter",
      "input": "score_details",
      "options": {
        "score_criteria_id": 4
      }
    },
    {
      "name": "psi_final",
      "method": "join",
      "input": "psi1_scores",
      "options": {
        "operation": "average",
        "properties": [
          "score_criteria_id"
        ]
      }
    }
  ]
}]
```

Below is full example for using formula from real scores collection
```go
package main

import (
	"os"

	"bitbucket.org/ofuro28/aimsisutils/formula"
	"bitbucket.org/ofuro28/aimsisutils/output"
	"bitbucket.org/ofuro28/aimsisutils/parse"
	"bitbucket.org/ofuro28/aimsisutils/score"
)

func main() {
	var (
		nodes         []formula.Node
		academicCycle score.AcademicCycle
	)
	scoreFinal := make(map[int]map[string][]score.Final)

	// Load formula and scores
	formulaPath := "nodes/score_final_with_KD.json"
	scorePath := "scores/300110000_1.json"
	parse.LoadJSON(formulaPath, &nodes)
	parse.LoadJSON(scorePath, &academicCycle)

	// Exec the formulas with given scores
	for _, scoreHeader := range academicCycle.ScoreHeaders {
		scoreFinal[scoreHeader.UserID] = formula.ExecNodes(&nodes, &scoreHeader, academicCycle.ScoreComponents)
	}

	output.DumpJSON(scoreFinal)
}
```

## Output
* **DumpJSON** - Print beautiful JSON data

## Parse 
* **LoadJSON** - Load JSON file and assign it to struct
* **SpecialTime** - special type to handling PostgreSQL datetime with timestamp