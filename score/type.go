package score

// Score Type Section

// AcademicCycle type ...
type AcademicCycle struct {
	AcademicCycleID  int                `json:"academic_cycle_id,omitempty"`
	AcademicPeriodID int                `json:"academic_period_id,omitempty"`
	SchoolID         int                `json:"school_id,omitempty"`
	SubjectID        int                `json:"subject_id,omitempty"`
	ClassID          int                `json:"class_id,omitempty"`
	FormulaID        int                `json:"formula_id,omitempty"`
	ScoreHeaders     []*Header          `json:"score_headers,omitempty"`
	ScoreComponents  map[int]*Component `json:"score_components,omitempty"`

	/* CreatedBy int               `json:"created_by,omitempty"`
	CreatedAt parse.SpecialTime `json:"created_at,omitempty"`
	UpdatedBy int               `json:"updated_by,omitempty"`
	UpdatedAt parse.SpecialTime `json:"updated_at,omitempty"`
	DeletedBy int               `json:"deleted_by,omitempty"`
	DeletedAt pq.NullTime       `json:"deleted_at,omitempty"` */
}

// Header type
type Header struct {
	ScoreHeaderID   int       `json:"score_header_id,omitempty"`
	AcademicCycleID int       `json:"academic_cycle_id,omitempty"`
	UserID          int       `json:"user_id,omitempty"`
	ScoreDetails    []*Detail `json:"score_details,omitempty"`

	/* CreatedBy int               `json:"created_by,omitempty"`
	CreatedAt parse.SpecialTime `json:"created_at,omitempty"`
	UpdatedBy int               `json:"updated_by,omitempty"`
	UpdatedAt parse.SpecialTime `json:"updated_at,omitempty"`
	DeletedBy int               `json:"deleted_by,omitempty"`
	DeletedAt pq.NullTime       `json:"deleted_at,omitempty"` */
}

// Detail type ....
type Detail struct {
	ScoreDetailID    int     `json:"score_detail_id,omitempty"`
	ScoreComponentID int     `json:"score_component_id,omitempty"`
	ScoreHeaderID    int     `json:"score_header_id,omitempty"`
	Score            float32 `json:"score,omitempty"`
	TeacherID        int     `json:"teacher_id,omitempty"`

	/* CreatedBy int               `json:"created_by,omitempty"`
	CreatedAt parse.SpecialTime `json:"created_at,omitempty"`
	UpdatedBy int               `json:"updated_by,omitempty"`
	UpdatedAt parse.SpecialTime `json:"updated_at,omitempty"`
	DeletedBy int               `json:"deleted_by,omitempty"`
	DeletedAt pq.NullTime       `json:"deleted_at,omitempty"` */
}

// Component type ...
type Component struct {
	ScoreComponentID int     `json:"score_component_id,omitempty"`
	AcademicCycleID  int     `json:"academic_cycle_id,omitempty"`
	ScoreCriteriaID  int     `json:"score_criteria_id,omitempty"`
	ScoreMethodID    int     `json:"score_method_id,omitempty"`
	Name             string  `json:"name,omitempty"`
	ScoreGroupingID1 int     `json:"score_grouping_id_1,omitempty"`
	ScoreGroupingID2 int     `json:"score_grouping_id_2,omitempty"`
	ScoreGroupingID3 int     `json:"score_grouping_id_3,omitempty"`
	SlugSK           int     `json:"slug_sk,omitempty"`
	SlugKD           string  `json:"slug_kd,omitempty"`
	SlugI            string  `json:"slug_i,omitempty"`
	Weight           float32 `json:"weight,omitempty"`
	KkmOverwrite     float32 `json:"kkm_overwrite,omitempty"`
	Description      string  `json:"description,omitempty"`
}

// Final type ..
type Final struct {
	Name             string  `json:"name,omitempty"`
	Score            float32 `json:"score,omitempty"`
	ScoreComponentID int     `json:"score_component_id,omitempty"`
	ScoreCriteriaID  int     `json:"score_criteria_id,omitempty"`
	ScoreMethodID    int     `json:"score_method_id,omitempty"`
	ScoreGroupingID1 int     `json:"score_grouping_id_1,omitempty"`
	ScoreGroupingID2 int     `json:"score_grouping_id_2,omitempty"`
	ScoreGroupingID3 int     `json:"score_grouping_id_3,omitempty"`
	SlugKD           string  `json:"slug_kd,omitempty"`
	SlugI            string  `json:"slug_i,omitempty"`
	TeacherID        int     `json:"teacher_id,omitempty"`
	Description      string  `json:"description,omitempty"`
}
