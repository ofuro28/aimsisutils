package score

// FlattenScore to convert score detail and score component to score final format
func FlattenScore(scoreDetails []*Detail, scoreComponents map[int]*Component) []*Final {
	var scoreFinals []*Final

	for _, scoreDetail := range scoreDetails {

		scoreComponent := scoreComponents[scoreDetail.ScoreComponentID]
		scoreFinals = append(scoreFinals, &Final{
			Name:             scoreComponent.Name,
			Score:            scoreDetail.Score,
			ScoreComponentID: scoreDetail.ScoreComponentID,
			ScoreCriteriaID:  scoreComponent.ScoreCriteriaID,
			ScoreMethodID:    scoreComponent.ScoreMethodID,
			ScoreGroupingID1: scoreComponent.ScoreGroupingID1,
			ScoreGroupingID2: scoreComponent.ScoreGroupingID2,
			ScoreGroupingID3: scoreComponent.ScoreGroupingID3,
			SlugKD:           scoreComponent.SlugKD,
			TeacherID:        scoreDetail.TeacherID,
			Description:      scoreComponent.Description,
		})

	}

	return scoreFinals
}
