package formula

import (
	"encoding/json"
	"fmt"
	"log"
	"math"
	"strconv"
	"strings"

	"bitbucket.org/ofuro28/aimsisutils/score"
)

// Init struct ...
type Init struct {
}

// ExecNodes to running formula nodes
func ExecNodes(nodes *[]Node, scoreHeader *score.Header, scoreComponents map[int]*score.Component) map[string][]*score.Final {
	var scoreOutput []*score.Final
	scoreFinal := make(map[string][]*score.Final)
	scores := make(map[string][]*score.Final)

	scoreFinal = make(map[string][]*score.Final)
	scores["score_details"] = score.FlattenScore(scoreHeader.ScoreDetails, scoreComponents)
	for _, node := range *nodes {

		if node.Input == "score_details" {
			scoreOutput = ExecFormulas(&node, scores)
		} else if node.Input == "score_finals" {
			scoreOutput = ExecFormulas(&node, scoreFinal)
		}
		scoreFinal[node.Name] = scoreOutput
	}

	return scoreFinal
}

// ExecFormulas to repeatedly processing formula node by it's sequence
func ExecFormulas(node *Node, scoreMap map[string][]*score.Final) (scoreOutput []*score.Final) {

	for _, formula := range node.Formulas {

		switch formula.Method {
		case "groupBy":
			// fmt.Println("Using grouping formula")
			scoreMap[formula.Name] = groupBy(formula.Options, scoreMap[formula.Input], formula.Name)
		case "filter":
			// fmt.Println("Using filter formula")
			scoreMap[formula.Name] = filter(formula.Options, scoreMap[formula.Input])
		case "join":
			// fmt.Println("Using average formula")
			scoreMap[formula.Name] = joinOp(formula.Options, scoreMap[formula.Input], formula.Name)
		case "rawMath":
			// fmt.Println("Using math formula")
			scoreMap[formula.Name] = rawMath(formula.Options, scoreMap, formula.Name)
		case "math":
			// fmt.Println("Using math formula")
			scoreMap[formula.Name] = mathOp(formula.Options, scoreMap[formula.Input], formula.Name)
		default:
			fmt.Println("Formula undefined")
		}
	}

	if node.Input == "score_finals" {
		for _, formula := range node.Formulas {
			if formula.Name != node.Name {
				delete(scoreMap, formula.Name)
			}
		}
	}

	scoreOutput = scoreMap[node.Name]

	return scoreOutput
}

// groupBy to grouping score by given key, and running sub-formula for each child
func groupBy(options *json.RawMessage, input []*score.Final, outputName string) []*score.Final {
	var (
		groupBy     GroupBy
		key         string
		scoreOutput []*score.Final
		// tempScores  []score.Final
		scoreFinal *score.Final
	)
	scoreMap := make(map[string][]*score.Final)
	groupedScore := make(map[string][]*score.Final)

	// Unmarsh options
	byteValue := []byte(*options)
	if err := json.Unmarshal(byteValue, &groupBy); err != nil {
		log.Fatal(err)
	}

	switch groupBy.ByKey {
	case "slug_kd":
		for _, score := range input {
			groupedScore[score.SlugKD] = append(groupedScore[score.SlugKD], score)
		}
	case "score_method_id":
		for _, score := range input {
			key = string(score.ScoreMethodID)
			groupedScore[key] = append(groupedScore[key], score)
		}
	case "score_criteria_id":
		for _, score := range input {
			key = string(score.ScoreMethodID)
			groupedScore[key] = append(groupedScore[key], score)
		}
	default:
		fmt.Println("Undefined key for grouping formula")
	}

	subNode := Node{
		Name:     groupBy.Name,
		Formulas: groupBy.Formulas,
	}
	for _, value := range groupedScore {
		scoreMap[groupBy.Name] = value
		ExecFormulas(&subNode, scoreMap)

		scoreFinal = scoreMap["group_final"][0]
		scoreOutput = append(scoreOutput, scoreFinal)
	}
	// output.DumpJSON(scoreOutput)

	return scoreOutput
}

// filter score by given params
func filter(options *json.RawMessage, input []*score.Final) []*score.Final {
	var (
		filter Filter
		output []*score.Final
	)

	// Unmarsh options
	byteValue := []byte(*options)
	if err := json.Unmarshal(byteValue, &filter); err != nil {
		log.Fatal(err)
	}

	// Filtering by score criteria, score method and slug KD
	for _, score := range input {
		if filter.ScoreCriteriaID != 0 {
			if filter.ScoreCriteriaID != score.ScoreCriteriaID {
				continue
			}
		}

		if filter.ScoreMethodID != 0 {
			if filter.ScoreMethodID != score.ScoreMethodID {
				continue
			}
		}

		if filter.SlugKD != "" {
			if filter.SlugKD != score.SlugKD {
				continue
			}
		}

		output = append(output, score)
	}

	return output
}

func rawMath(options *json.RawMessage, scoreMap map[string][]*score.Final, outputName string) []*score.Final {
	var (
		scoreOutput        []*score.Final
		scoreFinal         float64
		rawMath            RawMath
		operand1, operand2 float64
		err                error
		sf                 *score.Final
	)

	// Unmarsh options
	byteValue := []byte(*options)
	if err := json.Unmarshal(byteValue, &rawMath); err != nil {
		log.Fatal(err)
	}

	operations := strings.Split(rawMath.Operation, " ")

	for i := 1; i < len(operations); i += 2 {
		if i == 1 {
			if strings.Contains(operations[0], "#") {
				name := strings.Replace(operations[0], "#", "", 1)
				if len(scoreMap[name]) > 0 {
					operand1 = float64(scoreMap[name][0].Score)
					sf = scoreMap[name][0]
				}
			} else {
				operand1, err = strconv.ParseFloat(operations[0], 64)
				if err != nil {
					log.Fatal(err)
				}
			}
		}

		if strings.Contains(operations[i+1], "#") {
			name := strings.Replace(operations[i+1], "#", "", 1)
			if len(scoreMap[name]) > 0 {
				operand2 = float64(scoreMap[name][0].Score)
				sf = scoreMap[name][0]
			}
		} else {
			operand2, err = strconv.ParseFloat(operations[i+1], 64)
			if err != nil {
				log.Fatal(err)
			}
		}

		if operand1 != 0 && operand2 != 0 {
			switch operations[i] {
			case "+":
				scoreFinal = operand1 + operand2
			case "-":
				scoreFinal = operand1 - operand2
			case "*":
				scoreFinal = operand1 * operand2
			case "/":
				scoreFinal = operand1 / operand2
			case "%":
				scoreFinal = math.Mod(operand1, operand2)
			}

			operand1 = scoreFinal
		}
	}
	if sf != nil {
		sf.Score = float32(scoreFinal)
		sf.Name = outputName
	} else {
		sf = &score.Final{
			Score: float32(scoreFinal),
			Name:  outputName,
		}
	}

	scoreOutput = append(scoreOutput, sf)
	return scoreOutput
}

// joinOp is collection of function to merge scores into one
func joinOp(options *json.RawMessage, input []*score.Final, outputName string) []*score.Final {
	var (
		joinOp      JoinOperation
		scoreOutput []*score.Final
		scoreFinal  float32
		scoreSum    float32
		newMethod   int
		newCriteria int
		newKD       string
	)

	if len(input) > 0 {
		// Unmarsh options
		byteValue := []byte(*options)
		if err := json.Unmarshal(byteValue, &joinOp); err != nil {
			log.Fatal(err)
		}

		switch joinOp.Operation {
		case "average":
			// Get average scores
			count := float32(len(input))
			for _, score := range input {
				scoreSum += score.Score
			}
			scoreFinal = scoreSum / count
		case "count":
			// Get average scores
			count := float32(len(input))
			scoreFinal = count
		case "sum":
			// Get sum scores
			for _, score := range input {
				scoreSum += score.Score
			}
			scoreFinal = scoreSum
		case "max":
			// get max score
			scoreFinal = input[0].Score
			for _, score := range input {
				if score.Score > scoreFinal {
					scoreFinal = score.Score
				}
			}
		case "min":
			// get min score
			scoreFinal = input[0].Score
			for _, score := range input {
				if score.Score < scoreFinal {
					scoreFinal = score.Score
				}
			}
		}

		for _, prop := range joinOp.Properties {
			switch prop {
			case "score_criteria_id":
				newCriteria = input[0].ScoreCriteriaID
			case "score_method_id":
				newMethod = input[0].ScoreMethodID
			case "slug_kd":
				newKD = input[0].SlugKD
			}
		}

		var newName strings.Builder
		if outputName == "group_final" {
			newName.WriteString("final")
			if newCriteria != 0 {
				newName.WriteString("_criteria-" + strconv.Itoa(newCriteria))
			}
			if newMethod != 0 {
				newName.WriteString("_method-" + strconv.Itoa(newMethod))
			}
			if newKD != "" {
				newName.WriteString("_KD-" + newKD)
			}
		} else {
			newName.WriteString(outputName)
		}
		sf := input[0]
		sf.Name = newName.String()
		sf.ScoreCriteriaID = newCriteria
		sf.ScoreMethodID = newMethod
		sf.SlugKD = newKD
		sf.Score = scoreFinal
		scoreOutput = append(scoreOutput, sf)
	}

	return scoreOutput
}

// mathOp is collection of math operation for single value
func mathOp(options *json.RawMessage, input []*score.Final, outputName string) []*score.Final {
	var (
		scoreOutput []*score.Final
		scoreFinal  float64
		mathOp      MathOperation
	)

	if len(input) > 0 {
		// Unmarsh options
		byteValue := []byte(*options)
		if err := json.Unmarshal(byteValue, &mathOp); err != nil {
			log.Fatal(err)
		}

		scoreCurrent := float64(input[0].Score)

		switch mathOp.Operation {
		case "round":
			scoreFinal = mathRound(scoreCurrent, mathOp.Precision)
		case "roundDown":
			scoreFinal = mathRoundDown(scoreCurrent, mathOp.Precision)
		case "roundUp":
			scoreFinal = mathRoundUp(scoreCurrent, mathOp.Precision)
		default:
		}

		scoreOutput = input
		scoreOutput[0].Score = float32(scoreFinal)
		scoreOutput[0].Name = outputName
	}

	return scoreOutput
}

// mathRound is sub operation of mathOp to calculate nearest round score
func mathRound(input float64, precision int) (newVal float64) {
	pow := math.Pow(10, float64(precision))
	newVal = math.Round(input*pow) / pow
	return newVal
}

// mathRoundUp is sub operation of mathOp to round up score
func mathRoundUp(input float64, precision int) (newVal float64) {
	var round float64
	pow := math.Pow(10, float64(precision))
	digit := pow * input
	round = math.Ceil(digit)
	newVal = round / pow
	return
}

// mathRoundUp is sub operation of mathOp to round down score
func mathRoundDown(input float64, precision int) (newVal float64) {
	var round float64
	pow := math.Pow(10, float64(precision))
	digit := pow * input
	round = math.Floor(digit)
	newVal = round / pow
	return
}
