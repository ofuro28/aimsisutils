package formula

import (
	"encoding/json"
)

// Node type ...
type Node struct {
	Name     string    `json:"name,omitempty"`
	Formulas []Formula `json:"formulas,omitempty"`
	Input    string    `json:"input,omitempty"`
}

// Formula ...
type Formula struct {
	Name    string           `json:"name,omitempty"`
	Method  string           `json:"method,omitempty"`
	Input   string           `json:"input,omitempty"`
	Options *json.RawMessage `json:"options,omitempty"`
}

// Filter type ...
type Filter struct {
	ScoreCriteriaID  int    `json:"score_criteria_id,omitempty"`
	ScoreMethodID    int    `json:"score_method_id,omitempty"`
	ScoreGroupingID1 int    `json:"score_grouping_id_1,omitempty"`
	ScoreGroupingID2 int    `json:"score_grouping_id_2,omitempty"`
	ScoreGroupingID3 int    `json:"score_grouping_id_3,omitempty"`
	SlugKD           string `json:"slug_kd,omitempty"`
	SlugI            string `json:"slug_i,omitempty"`
}

// RawMath type ...
type RawMath struct {
	Operation string `json:"operation,omitempty"`
}

// MathOperation type ...
type MathOperation struct {
	Operation string `json:"operation,omitempty"`
	Precision int    `json:"precision,omitempty"`
}

// GroupBy type for grouping score to mapped group
type GroupBy struct {
	ByKey    string    `json:"by_key,omitempty"`
	Name     string    `json:"name,omitempty"`
	Formulas []Formula `json:"formulas,omitempty"`
}

// JoinOperation type ..
type JoinOperation struct {
	Operation  string   `json:"operation,omitempty"`
	Properties []string `json:"properties,omitempty"`
}
