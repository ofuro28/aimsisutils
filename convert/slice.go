package convert

import "strconv"

// IntToString will convert slices with member of int to string slices
func IntToString(slices []int) (newSlices []string) {
	for _, slice := range slices {
		newSlices = append(newSlices, strconv.Itoa(slice))
	}

	return
}
