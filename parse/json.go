package parse

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

// LoadJSON to load json file and parsing to struct
func LoadJSON(jsonStr string, nodes interface{}, fromFile bool) interface{} {
	var tempByte []byte
	if fromFile {
		jsonFile, err1 := os.Open(jsonStr)
		if err1 != nil {
			fmt.Println(err1)
		}
		tempByte, _ = ioutil.ReadAll(jsonFile)
		defer jsonFile.Close()
	} else {
		tempByte = []byte(jsonStr)
	}

	if err := json.Unmarshal(tempByte, &nodes); err != nil {
		log.Fatal(err)
	}

	return nodes
}

// ToStruct convert JSON byte to struct
func ToStruct(jsonByte []byte, nodes interface{}) interface{} {
	if err := json.Unmarshal(jsonByte, &nodes); err != nil {
		log.Fatal(err)
	}

	return nodes
}
