package parse

import (
	"strings"
	"time"

	"github.com/lib/pq"
)

// SpecialTime to parsing special date
type SpecialTime struct {
	time.Time
}

// UnmarshalJSON custom
func (sd *SpecialTime) UnmarshalJSON(input []byte) error {
	loc, _ := time.LoadLocation("Asia/Jakarta")

	strInput := string(input)
	strInput = strings.Trim(strInput, `"`)
	// newTime, err := time.Parse("2019-11-29 15:47:53.446763+0700", strInput, loc)
	newTime, err := pq.ParseTimestamp(loc, strInput)

	if err != nil {
		return err
	}

	sd.Time = newTime
	return nil
}
