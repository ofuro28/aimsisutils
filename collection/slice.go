package collection

func Splice(items []interface{}, value interface{}) []interface{} {

	index := -1
	for i, item := range items {
		if item == value {
			index = i
		}
	}
	if index != -1 {
		copy(items[index:], items[index+1:]) // Shift a[i+1:] left one index
		items[len(items)-1] = ""             // Erase last element (write zero value)
		items = items[:len(items)-1]         // Truncate slice
	}

	return items
}
